<?php
//seção home
Route::get('/', function () {
    return view('home.index');
});

Route::get('/contato', function () {
    return view('home.contato');
});
//seção notícia
Route::get('/tecnologia', function () {
    return view('noticias.index');
});

Route::get('/tecnologia/titulo-noticia', function () {
    return view('noticias.visualizar');
});

//Seção Admin
Route::get('/admin/home',function(){
    return view('admin.home.index');
})->name('home');

//seção admin/noticias

Route::get('/admin/noticias','Admin\NoticiasController@index')->name('noticias');

Route::get('/admin/noticias/cadastrar','Admin\NoticiasController@cadastrar')->name('cadastrar');

Route::get('/admin/noticias/editar','Admin\NoticiasController@editar')->name('editar');

Route::get('/admin/noticias/visualizar','Admin\NoticiasController@visualizar')->name('visualizar');

Route::get('/admin/noticias/deletar','Admin\NoticiasController@deletar')->name('deletar');

//seção Admin/categorias
Route::get('/admin/categorias','Admin\CategoriasController@index');

Route::get('/admin/categorias/visualizar','Admin\CategoriasController@visualizar');

Route::get('/admin/categorias/editar','Admin\CategoriasController@editar');

Route::get('/admin/categorias/cadastrar','Admin\CategoriasController@cadastrar');

Route::get('/admin/categorias/deletar','Admin\CategoriasController@deletar');

//seção Admin/usuários
Route::get('/admin/usuarios','Admin\UsuariosController@index');

Route::get('/admin/usuario/visualizar','Admin\UsuariosController@visualizar');

Route::get('/admin/usuario/cadastrar','Admin\UsuariosController@cadastrar');

Route::get('/admin/usuario/editar','Admin\UsuariosController@editar');

Route::get('/admin/usuario/deletar','Admin\UsuariosController@deletar');
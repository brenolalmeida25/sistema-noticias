<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsuariosController extends Controller
{
    public function index(){
        return view('admin.usuarios.index')->name('home');
    }
    public function visualizar(){
        return view('admin.usuarios.visualizar')->name('visualizar');
    }
    public function cadastrar(){
        return view('admin.usuarios.cadastrar')->name('cadastrar');
    }
    public function editar(){
        return view('admin.usuarios.editar')->name('editar');
    }
    public function deletar(){
        echo 'Função Deletar';
    }
   
}

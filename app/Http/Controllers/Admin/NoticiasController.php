<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NoticiasController extends Controller
{
    public function index(){
        return view('admin.noticias.index');
    }

    public function visualizar(){
        return view('admin.noticias.visualizar');
    }

    public function cadastrar(){
        return view('admin.noticias.cadastrar');
    }

    public function editar(){
        return view('admin.noticias.editar');
    }

    public function deletar(){
        echo 'Função deletar';
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarNoticiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo',150);
            $table->string('subtitulo',150)->nullable();
            $table->string('slug',150);
            $table->text('descricao');
            $table->tinyInteger('status')->default(0);
            $table->timestamp('dta_publicacao')->nullable();
            $table->timestamp('dta_atualizacao')->nullable();
            $table->bigInteger('categoria_id')->unsigned();
            $table->bigInteger('usuario_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->foreign('usuario_id')->references('id')->on('categorias');
        });
    }
    /*kore wa misu desu ka?*/
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticias');
    }
}

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>G1 - Notícias :: @yield('titulo')</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
</head>

<body>
    <header>
        <div class="cor-primaria barra-fixa">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-2">      

                        <img src="{{ asset('img/logo.jpg') }}" alt="G1" width="60">                     

                    </div>
                    <div class="col-md-7 col-5" id="form-busca">
                        <form action="#" method="get">
                            <div class="form-group">
                                <input type="text" placeholder="Pesquisar..." class="form-control">
                            </div>
                        </form>
                    </div>

                    <div class="col-md-2 col-4" id="btn-perfil">
                        <button class="dropdown-toggle" data-toggle="dropdown">
                            <img class="rounded-circle mr-1" src="https://via.placeholder.com/50x50" alt="perfil" width="30">
                            Breno
                        </button>
                        <div class="dropdown-menu">
                            <p class="dropdown-header">Admnistrador</p>
                            <a class="dropdown-item">Editar Perfil</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item">Sair</a>
                        </div>
                    </div>

                    <span class="btn-collapse-menu">
                        <i class="fas fa-bars"></i>
                    </span>

                </div>
            </div>
        </div>
        <div class="cor-secundaria collapse-menu collapse-menu-hide">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav id="menu">
                            <ul>
                                <li><a href="{{Route('home')}}">Home</a></li>
                                <li><a href="{{Route('noticias')}}">Notícias</a></li>
                                <li><a href="#">Categorias</a></li>
                                <li><a href="#">Contato</a></li>
                                <li><a href="#">Usuários</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <main class="mt-5">
        
        @yield('conteudo')

    </main>


    <footer class="mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>&copy; <?=date('Y') ?> ou {{ date('Y') }}  - Todos os direitos são reservados</p>
                </div>    
            </div>
        </div>

    </footer>
    <script src="{{ asset('js/admin.js') }}"></script>
</body>

</html>

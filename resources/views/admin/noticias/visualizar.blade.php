@extends ('layouts.admin')

@section('titulo','Área Admnistrativa')

@section('conteudo')

<div class="container">

    <div class="row">
        <div class="col-12">
            <h2>Visualizar</h2>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <table class="table table-striped table-condensed">
                <tr>
                    <td class="font-weight-bold" width="150">ID</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td class="font-weight-bold" width="150">Título</td>
                    <td>Flamengo ganhou do corinthians</td>
                </tr>
                <tr>
                    <td class="font-weight-bold" width="150">Subtítulo</td>
                    <td>e o corinthians vai perder no próximo jogo também</td>
                </tr>
                <tr>
                    <td class="font-weight-bold" width="150">Descrição</td>
                    <td>Hodor possui mais de dois metros e dez centímetros de altura, com olhos castanhos[1] e uma barba        castanhaÉ sugerido por Osha que seu grande tamanho seja resultado de sangue de gigantes.

                            Hodor é lento de raciocínio, mas é gentil e leal à família Stark. Ele só é capaz de dizer uma palavra, Hodor As crianças Stark acreditam que este seja o seu nome, até que a Velha Ama, sua bisavó, revela que seu verdadeiro nome é Walder.</td>
                </tr>
                <tr>
                    <td class="font-weight-bold" width="150">Status</td>
                    <td>Não Publicado</td>
                </tr>
            </table>
        </div>    
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <a href="#" class="btn btn-danger">Editar Notícia</a>
            <a href="#" class="btn btn-success">Cancelar</a>
        </div>
    </div>
</div>

@endsection
<form action="#" method="post">
    <div class="form-group row">
            <label class="col-sm-2 col-form-label text-right font-weight-bold" for="titulo">Titulo</label>
            <div class="col-sm-10">
            <input class="form-control" type="text" name="titulo"
            id="titulo" value="">
            </div>
    </div>
    <div class="form-group row">
            <label class="col-sm-2 col-form-label text-right font-weight-bold" for="subtitulo">Subtitulo</label>
            <div class="col-sm-10">
            <input class="form-control" type="text" name="subtitulo"
            id="subtitulo" value="">
            </div>
    </div>
    <div class="form-group row">
            <label class="col-sm-2 col-form-label text-right font-weight-bold" for="descricao">descricao</label>
            <div class="col-sm-10">
            <textarea class="form-control" type="text" name="descricao"
            id="descricao" value=""></textarea>
            </div>
    </div>
    <div class="form-group row">
            <label class="col-sm-2 col-form-label text-right font-weight-bold" for="status">status</label>
            <div class="col-sm-10">
                <select name="status" class="form-control">
                        <option value="0">Não Publicado</option>
                        <option value="1">Aguardando Publicação</option>
                        <option value="2">Publicado</option>
                </select>

            </div>
    </div>

    <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
            <button type="submit" class="btn btn-danger">Enviar</button>
            <a href="#" class="btn btn-secondary">Cancelar</a>
        </div>
    </div>

</form>
@extends ('layouts.admin')

@section ('conteudo')
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <table class="table table-bordered table-striped">
                        <thead class="table">
                            <tr>
                                <th>#</th>
                                <th>nome</th>
                                <th>status</th>
                                <th>Qtd de notícias</th>
                                <th>Data cadastro</th>
                                <th>Ação</th>
                            </tr>
                        </thead>
                        <tbody class="table">
                            <td></td>
                            <td></td>
                            <td><select class="form-control">
                                    <option value="0">Ativado</option>
                                    <option value="1">Inativado</option>
                                </select>
                            </td>
                            <td></td>
                            <td></td>
                            <td>
                                    <a href="#" class="btn btn-sm btn-secondary">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    <a href="#" class="btn btn-sm btn-warning">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <a href="#" class="btn  btn-sm btn-danger">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </td>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection
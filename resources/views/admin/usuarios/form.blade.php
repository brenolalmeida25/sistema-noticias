@extends ('layouts.admin')

@section ('conteudo')
<div class="container">
    <div class="row mt-auto">
        <div class="col-12">
            <form action="#" method="POST">
                    <div class="form-group row font-weight-bold">
                            <label for="nome">Nome:</label>
                            <input type="text" id="nome"
                            name="nome" class="form-group">
                    </div>
                    <div class="form-group row font-weight-bold">
                            <label for="email">E-Mail:</label>
                            <input type="email" id="email"
                            name="email" class="form-group">
                    </div>
                    <div class="form-group row font-weight-bold">
                            <label for="senha">Senha:</label>
                            <input type="password" id="senha"
                            name="senha" class="form-group">
                    </div>
                    <div class="form-group row font-weight-bold">
                        <label for="perfil">Perfil:</label>
                        <input type=text id="perfil"
                        name="perfil" class="form-group">
                    </div>
                    <div class="form-group row font-weight-bold">
                        <label for="foto">Foto:</label>
                        <input type="file" id="foto"
                        name="foto" class="form-group">
                    </div>
                    <div class="form-group row font-weight-bold">
                        <label for="permissao">Permissão:</label>
                        <select for="permissao" class="form-control">
                            <option value="0">Colaborador</option>
                            <option value="1">Editor</option>
                            <option value="2">Administrador</option>
                        </select>
                    </div>
                    <div class="form-group row font-weight-bold">
                        <label for="status">Status</label>
                        <select name="status" class="form-control">
                            <option value="0">Ativo</option>
                            <option value="1">Inativo</option>
                        </select>
                    </div>
                    <div class="form-group">
                            <button type="submit" class="btn btn-danger">
                                Enviar
                            </button>
                            <a href="#" class="btn btn-secondary">
                                Cancelar
                            </a>
                    </div>
            </form>
        </div>
    </div>
</div>
@endsection
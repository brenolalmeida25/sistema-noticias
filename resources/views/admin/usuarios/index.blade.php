@extends ('layouts.admin')

@section ('conteudo')

<div class="container">
    <div class="row mt-3">
        <div class="col-12">
                <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>nome</th>
                                <th>email</th>
                                <th>permissão</th>
                                <th>status</th>
                                <th>data cadastro</th>
                                <th>ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row"></td>
                                <td scope="row"></td>
                                        <td><select class="form-control">
                                                <option value="0">Ativado</option>
                                                <option value="1">Inativado</option>
                                            </select>
                                        </td>
                                <td>
                                    <select class="form-control">
                                        <option value="0">Colaborador</option>
                                        <option value="1">Editor</option>
                                        <option value="2">Adiministrador</option>
                                    </select>
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                        <a href="#" class="btn btn-sm btn-secondary">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <a href="#" class="btn btn-sm btn-warning">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="#" class="btn  btn-sm btn-danger">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                            </tr>
                        </tbody>
                    
                    </table>
        </div>
    </div>
</div>
@endsection